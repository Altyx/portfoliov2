import React from 'react';
import Menu from '../Menu';
import Footer from '../Component/footer';
import '../assets/stylesheets/contact.scss';

export default function Contact() {
  return (
    <>
      <Menu />
      <div className="contact">
        <h1>Me contacter</h1>
        <p>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Hic inventore odio ratione rerum veniam. Accusantium aut doloremque, eligendi enim esse ex in labore maiores omnis pariatur, rerum tempora tempore temporibus?</p>
        <form action="">
          <div>
            <div className={"inputs"}>
              <input type="text" id={"name"} required placeholder={"Nom & prénom"} />
              <input type="number" placeholder={"Téléphone"} />
              <input type="email" id={"email"} required placeholder={"Email"} />
            </div>
            <textarea name="description" id="description" required placeholder={"Votre demande"}></textarea>
          </div>
          <button type={"submit"}>Envoyer</button>
        </form>
      </div>
      <Footer />
    </>
  )
}