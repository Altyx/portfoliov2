import React from "react"
import { Link } from 'gatsby';
import Menu from "../Menu"
import '../assets/stylesheets/projet.scss';
import Notaire from '../assets/img/notaire.png';
import Footer from "../Component/footer"

export default function Projets() {
  return (
    <>
      <Menu />
      <div className="projects">
        <section className={"__header"}>
          <h1>Mes réalisations</h1>
          <p>Voici quelques projets sur lesquelles j'ai pu travaillé.</p>
        </section>
        <div>
          <section className={"_projects"}>
            <div className="project">
              <img src={Notaire} width={"100%"} alt="notaire" />
              <div className="data">
                <h2>Site pour notaire</h2>
                <p>Création de la maquette et intégration wordpress d'un site pour notaire</p>
                <p className="techno">Wordpress</p>
              </div>
            </div>            <div className="project">
              <img src={Notaire} width={"100%"} alt="notaire" />
              <div className="data">
                <h2>Site pour notaire</h2>
                <p>Création de la maquette et intégration wordpress d'un site pour notaire</p>
                <p className="techno">Wordpress</p>
              </div>
            </div>            <div className="project">
              <img src={Notaire} width={"100%"} alt="notaire" />
              <div className="data">
                <h2>Site pour notaire</h2>
                <p>Création de la maquette et intégration wordpress d'un site pour notaire</p>
                <p className="techno">Wordpress</p>
              </div>
            </div>            <div className="project">
              <img src={Notaire} width={"100%"} alt="notaire" />
              <div className="data">
                <h2>Site pour notaire</h2>
                <p>Création de la maquette et intégration wordpress d'un site pour notaire</p>
                <p className="techno">Wordpress</p>
              </div>
            </div>
          </section>
        </div>
        <Footer />
      </div>
    </>
  )
}
