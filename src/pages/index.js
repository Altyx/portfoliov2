import React from "react";
import '../assets/stylesheets/home.scss';
import Menu from '../Menu';
import Me2 from '../assets/img/me2.png';
import Skills from "../Component/skills"
import Footer from "../Component/footer"
import Service from "../Component/services"

export default function Home() {
  return <>
    <Menu />
    <div className={"home"}>
      <div className={"dialog"}>
        <div className="hi">
          Bonjour, je suis
        </div>
        <div className={"me"}>Samir Moutawakil</div>
        <div className={"job"}>Lorem ipsum dolor sit amet, consectetur adipisicing elit. Aperiam corporis culpa cum cumque delectus error fugit harum libero magni, minus neque nobis officiis possimus quibusdam quos ratione sequi sit veniam.</div>
      </div>
      <div className={"portrait"}>
        <img src={Me2} alt="Me" />
      </div>
    </div>
    <Service />
    <Skills />
    <Footer />
  </>
}
