import React from 'react';
import { Link } from 'gatsby'
import './assets/stylesheets/menu.scss';
import logo from './assets/img/logo.png';

export default function Menu() {
  return (
    <nav className={'header'}>
      <Link to={"/"} className="name"><img src={logo} alt="SM" /></Link>
      <div className="nav">
        <Link to={"/experience"}>Expériences</Link>
        <Link to={"/projets"}>Réalisations</Link>
        <Link to={"/contact"}>Contact</Link>
      </div>
    </nav>
  )
}
