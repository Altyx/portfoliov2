## Ceci est le portfolio de Samir Moutawakil
### Un développeur fullstack JavaScript (Node et react principalement)

Il a été développé avec [Gatsby](https://www.gatsbyjs.com).

Vous pouvez vous inspirer du design, mais svp ne faite pas juste un copier/coller :).

Lien vers le [portfolio](https://samir-moutawakil.fr).

